package com.klm.pursal.application;

import android.app.Application;

public class App extends Application {
    private static App instance;

    private StorageCommon storage;

    public StorageCommon getStorage() {
        return storage;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        init();
    }

    private void init() {
    }

    public static App getInstance() {
        return instance;
    }
}

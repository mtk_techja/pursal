package com.klm.pursal.application;

public final class StorageCommon {
    private String[] mF1Data;


    public StorageCommon() {
        clearData();
    }

    public void clearData() {
        mF1Data = null;
    }

    public void setmF1Data(String[] data) {
        mF1Data = data;
    }

    public String[] getmF1Data() {
        return mF1Data;
    }
}

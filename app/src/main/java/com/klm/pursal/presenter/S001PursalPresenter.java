package com.klm.pursal.presenter;

import com.klm.pursal.event.S001OnOnPursalCallBack;

public class S001PursalPresenter extends BasePresenter<S001OnOnPursalCallBack> {
    public S001PursalPresenter(S001OnOnPursalCallBack event) {
        super(event);
    }
}

package com.klm.pursal.presenter;


import com.klm.pursal.event.OnCallBack;

public abstract class BasePresenter<T extends OnCallBack> {

    protected T mListener;

    public BasePresenter(T event) {
        mListener = event;
    }
}

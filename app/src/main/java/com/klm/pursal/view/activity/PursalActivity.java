package com.klm.pursal.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.klm.pursal.R;
import com.klm.pursal.event.S001OnOnPursalCallBack;
import com.klm.pursal.presenter.S001PursalPresenter;
import com.klm.pursal.view.base.BaseActivity;
import com.klm.pursal.view.entities.Account;
import com.klm.pursal.view.fragment.F001SplashFrg;
import com.klm.pursal.view.fragment.F002LoginFrg;
import com.klm.pursal.view.fragment.F003RegisterFrg;
import com.klm.pursal.view.fragment.F004ProductFrg;

public class PursalActivity extends BaseActivity<S001PursalPresenter> implements S001OnOnPursalCallBack {

    @Override
    protected void initPresenter() {
        mPresenter = new S001PursalPresenter(this);
    }

    @Override
    protected void initViews() {
        showFrg(R.id.ln_main, F001SplashFrg.TAG);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_main;
    }

    @Override
    public void callBack(Object data, String tag) {
        switch (tag) {
            case F001SplashFrg.SHOW_LOGIN_FRG:
                showFrg(R.id.ln_main, F002LoginFrg.TAG);
                break;
            case F002LoginFrg.KEY_SHOW_REGISTER:
                showFrg(R.id.ln_main, F003RegisterFrg.TAG);
                break;
            case F002LoginFrg.KEY_SHOW_PRODUCT_FRG:
                showFrg(R.id.ln_main, F004ProductFrg.TAG);
                break;
            default:
                Toast.makeText(this, "Pursal", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}

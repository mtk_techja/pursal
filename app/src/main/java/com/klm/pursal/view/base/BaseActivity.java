package com.klm.pursal.view.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.klm.pursal.presenter.BasePresenter;
import com.klm.pursal.view.OnActionCallBack;

import java.lang.reflect.Constructor;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements OnActionCallBack {

    protected T mPresenter;

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutId());
        initViews();
        initPresenter();
    }

    protected abstract void initPresenter();

    protected abstract void initViews();

    protected abstract int getLayoutId();

    public <T extends View> T findViewById(int id, View.OnClickListener event) {
        T v = super.findViewById(id);
        if (event != null) {
            v.setOnClickListener(event);
        }
        return v;
    }

    protected final void showFrg(int layoutMain, String tag) {
        try {
            Class<?> clazz = Class.forName(tag);
            Constructor<?> constructor = clazz.getConstructor();
            BaseFragment obj = (BaseFragment) constructor.newInstance();
            getSupportFragmentManager().beginTransaction().replace(layoutMain, obj, tag).commit();
            obj.setOnActionCallBack(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

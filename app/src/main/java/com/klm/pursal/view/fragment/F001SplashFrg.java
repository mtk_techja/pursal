package com.klm.pursal.view.fragment;

import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.klm.pursal.R;
import com.klm.pursal.event.F001OnCallBack;
import com.klm.pursal.presenter.F001SplashFrgPresenter;
import com.klm.pursal.view.base.BaseFragment;


public class F001SplashFrg extends BaseFragment<F001SplashFrgPresenter> implements F001OnCallBack {

    public static final String TAG = F001SplashFrg.class.getName();
    public static final String SHOW_LOGIN_FRG = "SHOW_LOGIN_FRG";
    private ImageView ivPhoto;

    @Override
    protected void initViews() {
        ivPhoto = findViewById(R.id.iv_photo, this);
        Animation animAlpha = AnimationUtils.loadAnimation(mContext, R.anim.anim_set1);
        ivPhoto.startAnimation(animAlpha);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showLoginFrg();
            }
        }, 5000);
    }

    private void showLoginFrg() {
        mCallBack.callBack(null, SHOW_LOGIN_FRG);
    }

    @Override
    protected void initPresenter() {
        mPresenter = new F001SplashFrgPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_splash;
    }
}

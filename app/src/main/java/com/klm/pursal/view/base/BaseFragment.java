package com.klm.pursal.view.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.klm.pursal.presenter.BasePresenter;
import com.klm.pursal.view.OnActionCallBack;

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements View.OnClickListener {
    protected Context mContext;
    protected View rootView;
    protected T mPresenter;
    protected OnActionCallBack mCallBack;

    public void setOnActionCallBack(OnActionCallBack event) {
        mCallBack = event;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutId(), container, false);
        initPresenter();
        initViews();
        return rootView;
    }

    protected abstract void initViews();

    protected abstract void initPresenter();

    protected abstract int getLayoutId();

    public <T extends View> T findViewById(int id, View.OnClickListener event) {
        T v = rootView.findViewById(id);
        if (event != null) {
            v.setOnClickListener(event);
        }

        return v;
    }

    public <T extends TextInputLayout> T findViewById(int id) {
        T v = rootView.findViewById(id);
        return v;
    }


    @Override
    public void onClick(View v) {
        //Do something
    }
}

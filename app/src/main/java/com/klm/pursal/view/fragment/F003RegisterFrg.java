package com.klm.pursal.view.fragment;

import android.view.View;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.klm.pursal.R;
import com.klm.pursal.event.F003OnCallBack;
import com.klm.pursal.presenter.F003RegisterFrgPresenter;
import com.klm.pursal.view.base.BaseFragment;

public class F003RegisterFrg extends BaseFragment<F003RegisterFrgPresenter> implements F003OnCallBack {
    public final static String TAG = F003RegisterFrg.class.getName();
    private static final String ACTION_REGISTER = "ACTION_REGISTER";
    public static final String KEY_ACCOUNT = "KEY_ACCOUNT";
    private TextInputEditText edtUser, edtPassWord, edtConfirm;
    private MaterialButton btRegis;

    @Override
    protected void initViews() {
        edtUser = findViewById(R.id.edt_username, this);
        edtPassWord = findViewById(R.id.edt_password, this);
        edtConfirm = findViewById(R.id.edt_confirm_password, this);
        btRegis = findViewById(R.id.bt_register, this);
    }

    @Override
    protected void initPresenter() {
        mPresenter = new F003RegisterFrgPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_register;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_register:
                checkAcc();
                //registerAct();
                break;
            default:
                break;

        }
    }

    private void checkAcc() {
        if (edtUser.getText().toString().isEmpty() || edtPassWord.getText().toString().isEmpty() || edtConfirm.getText().toString().isEmpty()) {
            Toast.makeText(mContext, "Not enough data", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!edtConfirm.getText().toString().equals(edtPassWord.getText().toString())) {
            Toast.makeText(mContext, "Wrong Pass", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(mContext, "Register Success", Toast.LENGTH_SHORT).show();
    }

    private void registerAct() {
        Toast.makeText(mContext, "Regis", Toast.LENGTH_SHORT).show();
    }
}

package com.klm.pursal.view.fragment;

import com.klm.pursal.R;
import com.klm.pursal.event.F004OnCallBack;
import com.klm.pursal.presenter.F004ProductFrgPresenter;
import com.klm.pursal.view.base.BaseFragment;

public class F004ProductFrg extends BaseFragment<F004ProductFrgPresenter> implements F004OnCallBack {
    public static final String TAG= F004ProductFrg.class.getName();
    @Override
    protected void initViews() {

    }

    @Override
    protected void initPresenter() {
        mPresenter = new F004ProductFrgPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_product;
    }
}

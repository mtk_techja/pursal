package com.klm.pursal.view.fragment;

import android.text.Editable;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.klm.pursal.R;
import com.klm.pursal.event.F002OnCallBack;
import com.klm.pursal.presenter.F002LoginFrgPresenter;
import com.klm.pursal.view.base.BaseFragment;


public class F002LoginFrg extends BaseFragment<F002LoginFrgPresenter> implements F002OnCallBack {
    public static final String TAG = F002LoginFrg.class.getName();
    public static final String KEY_SHOW_REGISTER = "KEY_SHOW_REGISTER";
    public static final String KEY_SHOW_PRODUCT_FRG = "KEY_SHOW_PRODUCT_FRG";
    private MaterialButton mButtonRegis, mButtonLogin;
    private TextInputLayout tipPassword;
    private TextInputEditText edtPassword;


    @Override
    protected void initViews() {
        mButtonRegis = findViewById(R.id.bt_register, this);
        mButtonLogin = findViewById(R.id.bt_login, this);
        tipPassword = findViewById(R.id.tip_password_login);
        tipPassword.setOnClickListener(this);
        edtPassword = findViewById(R.id.edt_password_login, this);

    }

    @Override
    protected void initPresenter() {
        mPresenter = new F002LoginFrgPresenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_login;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_register:
                mCallBack.callBack(null, KEY_SHOW_REGISTER);
                break;
            case R.id.bt_login:
                checkAcc();
                break;
            default:
                Toast.makeText(mContext, "Show regis", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void checkAcc() {
        if (!isPasswordValid(edtPassword.getText())) {
            tipPassword.setError("Password must contain at least 8 character.");
        } else {
            tipPassword.setError(null);
            mCallBack.callBack(null, KEY_SHOW_PRODUCT_FRG);
        }
    }

    private boolean isPasswordValid(Editable text) {
        return text != null && text.length() >= 8;
    }
}
